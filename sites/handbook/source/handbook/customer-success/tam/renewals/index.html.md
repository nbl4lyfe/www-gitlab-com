---
layout: handbook-page-toc
title: "Customer Renewal Tracking"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [TAM Handbook homepage](/handbook/customer-success/tam/) for additional TAM-related handbook pages.
Link to [Commercial Renewal Process](/handbook/customer-success/comm-sales/renewals/)
---

A key part of the customer relationship lifecycle is the renewal phase.  TAMs must proactively track the renewal dates of their customers and align with their Strategic Account Manager (SAL) or Account Executive (AE)  to ensure that a customer renewal takes place.

## Timeframe

Four months before the renewal date for a customer, a CTA will trigger in Gainsight, prompting the TAM to reach out to their aligned account team to discuss strategy and next steps for the renewal.  As part of this process, it will often be decided that the TAM will ask the ['soft'](https://www.mbaskool.com/business-concepts/marketing-and-strategy-terms/7214-soft-fact-questions.html) question around the renewal to see if there is any risk in the account and to provide time to mitigate any risk.  The expectation is that the team will meet internally and speak with the customer regarding the renewal within 30 days, leaving 90 days to run the renewal process.

## TAM Prioritization

On the Gainsight Attributes section, the TAM can set the priority level per customer with levels 1-3, with 1 being the highest. Definitions include:

1. High Touch: TAM-led onboarding, regular cadence calls, full TAM-led customer lifecycle
2. Medium Touch: TAM-led onboarding, quarterly check-ins, renewal touch point
3. Digital Touch: No direct TAM involvement, onboarding and enablement driven by email-led digital journey

Why do we use a prioritization system?

* Use in SALSATAM meetings to ensure team alignment
* If everything is important, than nothing is important. TAMs and Managers have visibility into how much attention a particular client should or is receiving, and provide a talking point to discuss when/if things change
* Managers have more visibility into the potential workload of the team member via more context on the makeup of the overall portfolio
* TAMs have more insight into their portfolio and where they should be spending their time
* TAMs can run associated campaigns of education to low touch or less engaged customers or strategic (high touch) customers to be automatically invited to Customer Advisory Boards or events
* Used to further segmentat customers, beyond the [Sales Segmentation](/handbook/business-ops/resources/#segmentation)

Criteria to apply includes:
- Future account growth
- Size of the account 
- Risk / health of account

The `TAM Portfolio` Dashboard is used to help highlight and review each client, including their priority level.


## Renewal Review Meeting

A “Renewal Review” meeting should have the following attendees:

- Strategic Account Leader
- Technical Account Manager
- Solutions Architect (if an upsell is being discussed)


The agenda of a “Renewal Review” meeting should include at least the following:

 1. Review of the customer's business objectives as documented in the success plan, and progress/completion of these goals
 1. Review of the customer health score and any changes over the past few months - changes can be seen in the Timeline in Gainsight.
 1. Review of support issues and the underlying reasons for any escalations.
 1. Review of high priority feature requests.
 1. Review of the customer's utilization of the product.
 1. Review of any known risk in the account

From this meeting a set of action items should be created to improve customer utilization and satisfaction with the product.  These items can include:

 1. **Architecture review** with Professional Services to address any underlying architectural weaknesses that could have contributed to an “Urgent” support escalation.
 1. **Product utilization review** to explore GitLab functionally that the customer is not using but could benefit from.
 1. **Roadmap review** to show the customer features that will be added to the product in the near term that may be valuable to them.  This could include a discussion with Product Management for strategic customers.

## Customer Cadence

The action items created from the “Renewal Review” meeting should be incorporated into the TAM customer cadence meetings and into any pending QBRs. The TAM should prioritize these reviews early in the renewal horizon.
